import axios from 'axios'


// ================= DEFINE BASE URL FROM ENV ==================== //
const baseAxios = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_BASE_URL
})


// ==================== INSERT API KEY FOR EVERY REQUEST ================== //
baseAxios.interceptors.request.use(async (config) => {
    config.params = {...config.params, api_key: process.env.NEXT_PUBLIC_API_KEY}
    return config
}, function (error) {
    return Promise.reject(error)
})

// =================== RESPONSE INTERCEPTOR ==================== //
baseAxios.interceptors.response.use((response) => {
    return response.data ? response.data : response
}, (error) => {
    const { response = {} } = error
    const { data } = response
    if (response?.status === 401) {
        return Promise.reject(data)
    }
    return Promise.reject(data)
})

export default baseAxios