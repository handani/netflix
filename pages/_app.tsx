import 'styles/css/tailwind.css'

import type { AppProps } from 'next/app'

// ================ AXIOS INTERCEPTOR ===================== //
import baseAxios from '../libs/axios'
import { AxiosInstance } from 'axios';

// =============== DEClARE AXIOS AS GLOBAL VARIABLE ==================== //
declare global {
    var $baseAxios: AxiosInstance
}

global.$baseAxios = baseAxios



function MyApp({ Component, pageProps }: AppProps) {
    return <Component {...pageProps} />
}
  
export default MyApp