import { NextPage } from "next";
import SectionMovies from "../components/home/section_movies.module";
import FullLayout from "../layouts/full";

const Home:NextPage = (props:any) => {
    const { genres } = props

    return (
        <FullLayout>
            <div className="flex flex-col relative">
                <section className="flex flex-col gap-2 px-[3%] sm:mb-5 md:mb-0 mb-5">
                    <h1 className="mt-[5vw]">Movies</h1>
                    <p className="sm:text-base lg:text-lg text-base sm:w-full md:w-3/4 lg:w-3/5 xl:w-2/5 w-full">Movies move us like nothing else can, whether they’re scary, funny,
                        dramatic, romantic or anywhere in-between. So many titles, so much to
                            experience.</p>
                </section>

                {/* ================ MOVIES AND GENRES SECTION =================== */}

                {
                    genres.map((genre:any, n:number) => <SectionMovies genre={genre} key={n} />)
                }
                
                {/* ================= END HERE ===================== */}

                <section className="relative text-center flex flex-col gap-5 relative gradient-blur">
                    <h2>There’s even more to watch.</h2>
                    <p className="sm:w-full lg:w-1/2 w-full sm:px-5 lg:px-0 px-5 mx-auto sm:text-base lg:text-lg text-base">Netflix has an extensive library of feature films, documentaries, TV shows, anime, award-winning Netflix originals, and more. Watch as much as you want, anytime you want.</p>
                    <button className="btn-red w-fit px-10 rounded-[2px] text-xl mx-auto py-3">JOIN NOW</button>
                </section>
            </div>
        </FullLayout>
    )
}

// ===================== GET MOVEIS ============================= //
const getMovies = async (id:number) => {
    var res:any = await global.$baseAxios.get('/discover/movie', { params: { language: 'en-US', include_video: true, page: 1, with_genres: id } }) 
    return res.results
}

// ================ GET MOVIES & GENRES ==================== //
export async function getServerSideProps () {
    var res:any = await global.$baseAxios.get('/genre/movie/list', { params: { language: 'en-US' } })
    const genres = res.genres.slice(0, 7)
    const promises = genres.map(async (genre:any) => genre.movies = [...await getMovies(genre.id)])
    await Promise.all(promises)
    return { props: { genres } }
}

export default Home