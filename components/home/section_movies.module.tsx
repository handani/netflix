import { NextPage } from "next"
import Image from "next/image"
import { IconChevronLeft, IconChevronRight } from '@tabler/icons'
import { useRef, useState, useEffect } from "react"

interface SectionMoviesProps {
    genre: any
}

const SectionMovies:NextPage<SectionMoviesProps> = (props) => {
    const { genre } = props
    const ref = useRef(null)
    const [prevButton, setPrevButton] = useState(true)
    const [nextButton, setNextButton] = useState(true)

    // ================== SCROLL FUNCTION ================ //
    const handleScroll = () => {
        var el = ref.current as any
        var scrollWidth:number = el.scrollWidth
        var scrolLeft:number = el.scrollLeft
        var scrollContainer:number = el.offsetWidth

        // ==================== VALIDATE SCROLL LEFT POSITION =================== //
        if (scrolLeft <= 0) setPrevButton(false)
        else setPrevButton(true)

        // ================= VALIDATE SCROLL RIGHT POSITION ====================== //
        if (scrolLeft + scrollContainer >= scrollWidth) setNextButton(false)
        else setNextButton(true)
    }

    useEffect(() => {
        handleScroll()
    }, [])

    // =========================== NAVIGATE FUNTION ==================== //
    const handleNavigate = (type:string) => {
        var el = ref.current as any
        var scrollLeft:number = el.scrollLeft
        var scrollContainer:number = el.offsetWidth

        // ===================== CHECK NEXT ITEM POSITION =================== //
        var nextPosition = type === 'next' ? scrollLeft + scrollContainer : scrollLeft - scrollContainer

        var movies = document.getElementsByClassName('movie')
        var nextMovie = Math.ceil(nextPosition /= movies[0].clientWidth + 12)

        nextMovie = type === 'next' ? nextMovie - 1 : nextMovie + 1

        if (nextMovie < 0) nextMovie = 0
        else if (nextMovie > movies.length - 1) nextMovie = movies.length - 1
        var element = movies[nextMovie] as any

        // =================== SCROLL TO NEXT ITEM POSITION ================ //
        el.scrollTo({ behavior: "smooth", left: element.offsetLeft })
    }


    return (
        <div className="genre flex justify-center sm:mt-[20px] lg:mt-[40px] mt-0 select-none px-[3%]">
            <div className="flex flex-col gap-2 w-full relative">
                <span>{genre.name}</span>

                {/* ================= MOVIES LIST ======================== */}
                <div ref={ref as React.RefObject<HTMLDivElement>} className="flex overflow-auto fancy-scroll relative" onScroll={handleScroll}>
                    <div className="flex flex-nowrap gap-3">
                        {
                            genre.movies.map((movie:any, n:number) => {
                                return (
                                    <div key={genre.name + '_' + n} className="relative cursor-pointer movie sm:w-[40vw] md:w-[28vw] lg:w-[22vw] xl:w-[300px] w-[40vw] text-center flex flex-col gap-2">
                                        <div className="sm:h-[22vw] md:h-[15vw] lg:h-[13vw] xl:h-[12vw] 2xl:h-[10vw] h-[22vw] w-full relative">
                                            <Image layout="fill" unoptimized={true} src={process.env.NEXT_PUBLIC_IMAGE_BASE_URL + movie.backdrop_path} alt={movie.title} />
                                        </div>
                                        <span className="text-white">{movie.title}</span>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>

                {/* ============== LEFT NAVIGATION =========== */}
                <button className={"text-primary-main absolute top-[45%] left-[-25px] " + (prevButton ? "sm:hidden lg:flex hidden" : "hidden")} onClick={() => handleNavigate('prev')}>
                    <IconChevronLeft />
                </button>

                {/* ============= RIGHT NAVIGATION ============= */}
                <button className={"text-primary-main absolute top-[45%] right-[-25px] " + (nextButton ? "sm:hidden lg:flex hidden" : "hidden")} onClick={() => handleNavigate('next')}>
                    <IconChevronRight />
                </button>
            </div>
        </div>
    )
}

export default SectionMovies