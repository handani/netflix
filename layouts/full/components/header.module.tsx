import { NextPage } from "next"
import Netflix from '../../../assets/svg/netflix.svg'
import NetflixSmall from '../../../assets/svg/netflix-small.svg'

const HeaderModule:NextPage = () => {
    return (
        <header className="fixed left-0 top-0 w-full z-10 text-white h-[62px]">
            <div className="px-[3%] bg-black flex justify-between h-full self-center py-3">
                <div className="sm:flex md:hidden flex h-full fill-primary-main my-auto">
                    <NetflixSmall />
                </div>
                
                <div className="sm:hidden md:flex hidden h-full fill-primary-main my-auto">
                    <Netflix />
                </div>
                <div className="flex gap-4">
                    <span className="my-auto text-sm sm:hidden lg:flex hidden">UNLIMITED TV SHOWS & MOVIES</span>
                    <button className="btn-red px-5 py-2 rounded-[2px] text-sm">JOIN NOW</button>
                    <button className="border border-solid border-gray-50/50 px-5 py-2 rounded-[2px] hover:bg-gray-200 hover:bg-opacity-20 text-sm">SIGN IN</button>
                </div>
            </div>
        </header>
    )
}

export default HeaderModule