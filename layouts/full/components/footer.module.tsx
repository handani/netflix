import { NextPage } from "next"
import { IconWorld } from '@tabler/icons'

const FooterModule:NextPage = () => {
    return (
        <footer className="w-11/12 py-8 mx-auto text-[#737373] flex flex-col gap-5">
            <a className="hover:underline underline-offset-2 text-[1em]" href="https://help.netflix.com/contactus">Questions? Contact us.</a>
            <div className="grid sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 grid-cols-2 gap-5 sm:w-full xl:w-3/5 w-full text-sm">
                    <a className="hover:underline underline-offset-2" href="https://help.netflix.com/support/412">FAQ</a>
                    <a className="hover:underline underline-offset-2" href="https://help.netflix.com">Help Center</a>
                    <a className="hover:underline underline-offset-2" href="/youraccount">Account</a>
                    <a className="hover:underline underline-offset-2" href="https://media.netflix.com/">Media Center</a>
                    <a className="hover:underline underline-offset-2" href="http://ir.netflix.com/">Investor Relations</a>
                    <a className="hover:underline underline-offset-2" href="https://jobs.netflix.com/jobs">Jobs</a>
                    <a className="hover:underline underline-offset-2" href="/redeem">Redeem Gift Cards</a>
                    <a className="hover:underline underline-offset-2" href="/gift-cards">Buy Gift Cards</a>
                    <a className="hover:underline underline-offset-2" href="/watch">Ways to Watch</a>
                    <a className="hover:underline underline-offset-2" href="https://help.netflix.com/legal/termsofuse">Terms of Use</a>
                    <a className="hover:underline underline-offset-2" href="https://help.netflix.com/legal/privacy">Privacy</a>
                    <a className="hover:underline underline-offset-2" href="#">Cookie Preferences</a>
                    <a className="hover:underline underline-offset-2" href="https://help.netflix.com/legal/corpinfo">Corporate Information</a>
                    <a className="hover:underline underline-offset-2" href="https://help.netflix.com/contactus">Contact Us</a>
                    <a className="hover:underline underline-offset-2" href="https://fast.com">Speed Test</a>
                    <a className="hover:underline underline-offset-2" href="https://help.netflix.com/legal/notices">Legal Notices</a>
                    <a className="hover:underline underline-offset-2" href="https://help.netflix.com/legal/corpinfo">Only on Netflix</a>
            </div>
            <div className="relative w-[200px]">
                <div className="absolute left-0 top-0 h-full flex flex-col justify-center px-4">
                    <IconWorld width={20} height={20} />
                </div>
                <select className="p-3 pl-12 select-none bg-black border border-solid border-white border-opacity-20 focus:border-opacity-100 text-sm rounded-[2px] w-full">
                    <option value="">English</option>
                    <option value="">Bahasa Indonesia</option>
                </select>
            </div>
        </footer>
    )
}

export default FooterModule