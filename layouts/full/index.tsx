import { NextPage } from "next"
import FooterModule from "./components/footer.module"
import HeaderModule from "./components/header.module"

interface FulllayoutProps {
    children: JSX.Element | JSX.Element[]
}

const FullLayout:NextPage<FulllayoutProps> = (props) => {
    const { children } = props
    return (
        <>
            <HeaderModule />
            <main className="bg-[#181818] w-full text-white min-h-screen pt-[62px]">
                {children}
                <FooterModule />
            </main>
        </>
    )
}

export default FullLayout