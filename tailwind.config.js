/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./layouts/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'primary-200': 'rgba(var(--primary-200), <alpha-value>)',
        'primary-main': 'rgba(var(--primary-main), <alpha-value>)',
      }
    },
  },
  plugins: [],
}
